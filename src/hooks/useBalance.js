import {useLayoutEffect, useEffect, useState } from 'react';
import { getBalance } from '../config/crud';

const useBalance = () => {
    const [balance, setBalance] = useState(0);

    useLayoutEffect(() => {
        async function loadBalance() {
            const value = await getBalance();
            setBalance(value);
        }
        loadBalance();
    }, [])
    // console.log( 'balance:', balance);
    return balance;
}

export default useBalance;