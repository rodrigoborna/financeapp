import { useDebugValue, useLayoutEffect, useEffect, useState } from 'react';
import { getIncome } from '../config/crud';

const useIncome = (startDate, endDate) => {
    const [income, setIncome] = useState(0);
    useEffect(() => {
        async function loadIncome() {
            const value = await getIncome(startDate, endDate);
            setIncome(value);
        }
        loadIncome();
    }, [startDate, endDate])
    // console.log( 'income: ', income);
    return income;
}

export default useIncome;