import { useDebugValue, useLayoutEffect, useEffect, useState } from 'react';
import { getExpense } from '../config/crud';

const useExpense = (startDate, endDate) => {
    const [expense, setExpense] = useState(0);

    useEffect(() => {
        async function loadExpense() {
            const value = await getExpense(startDate, endDate);
            setExpense(value);
        }
        loadExpense();
    }, [startDate, endDate])
    // console.log( 'expense:', expense);
    return expense;
}

export default useExpense;