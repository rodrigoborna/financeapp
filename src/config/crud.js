import firebase from './firebase';

export async function insertUser(userId, name, password, email, movement) {
  firebase.database().ref(userWay + userId).set(
    {
      name: name,
      password: password,
      email: email,
    }
  )
}

export async function insertMoviment(userId, description, value, movementDate
  , realizedDate, category, wallet, tag) {

  var movementKey = firebase.database().ref().child(userWay + userId).child(movementWay).push().key

  firebase.database().ref(userWay + userId).child(movementWay + movementKey).set(
    {
      description: description,
      value: value,
      movementDate: movementDate,
      realizedDate: realizedDate,
      category: category,
      wallet: wallet,
      tag: tag
    }
  )
}


export async function getBalance() {
  return await firebase.database().ref('users/1/movement')
    .orderByChild('realizedDate')
    .once('value')
    .then(function(snapshot) {
      let data=0;
      snapshot.forEach((object) => {
        data += object.child('value').val();
      })
      console.log('getBalance');
      return data;
    })
    .catch(function(error){
      return error.code;
    })
}

export async function getIncome(startDate, endDate) {
  return await firebase.database().ref('users/1/movement')
    .orderByChild('realizedDate').startAt(startDate).endAt(endDate)
    .once('value')
    .then(function(snapshot) {
      let data=0;
      snapshot.forEach((object) => {
        if (object.child('value').val() > 0) { data += object.child('value').val() };
      })
      console.log('getIncome');
      return data;
    })
    .catch(function(error){
      return error.code;
    })
}


export async function getExpense(startDate, endDate) {
  return await firebase.database().ref('users/1/movement')
    .orderByChild('realizedDate').startAt(startDate).endAt(endDate)
    .once('value')
    .then(function(snapshot) {
      let data=0;
      snapshot.forEach((object) => {
        if (object.child('value').val() < 0) { data += object.child('value').val() };
      })
      console.log('getExpense');
      return data;
    })
    .catch(function(error){
      return error.code;
    })
}