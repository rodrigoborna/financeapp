import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyBHPcwnqTkgX8ModUMP3qOXI_x9RUw5Mks",
  authDomain: "financialapp-4cba4.firebaseapp.com",
  databaseURL: "https://financialapp-4cba4-default-rtdb.firebaseio.com",
  projectId: "financialapp-4cba4",
  storageBucket: "financialapp-4cba4.appspot.com",
  messagingSenderId: "796844171390",
  appId: "1:796844171390:web:feeb308b452655ca2ff82f",
  measurementId: "G-5DFWN5L737"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export default firebase;