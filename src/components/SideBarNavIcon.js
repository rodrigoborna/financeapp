import React from 'react';
import { TouchableOpacity,StyleSheet} from 'react-native';
import { FontAwesome5 } from "@expo/vector-icons";

const SideBarNavIcon = (props) => {

    return (
        <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
          <FontAwesome5 name="bars" size={30} color="#161924" />
        </TouchableOpacity>
    );
}


export default SideBarNavIcon;