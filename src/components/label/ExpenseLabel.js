import React from 'react';

import useExpense from '../../hooks/useExpense';
import FormatCoin from '../../functions/formatCoin';

const ExpenseLabel = (props) => {
var expense = useExpense(props.startMonth, props.endMonth);
    return (
        <FormatCoin value={expense} />
    )
}

export default ExpenseLabel;