import React from 'react';

import useIncome from '../../hooks/useIncome';
import FormatCoin from '../../functions/formatCoin';

const IncomeLabel = (props) => {
let income = useIncome(props.startMonth, props.endMonth);
    return (
        <FormatCoin value={income} />
    )
}

export default IncomeLabel;