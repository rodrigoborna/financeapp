import React from 'react';

import useBalance from '../../hooks/useBalance';
import FormatCoin from '../../functions/formatCoin';

const BalanceLabel = () => {
var balance = useBalance();
    return (
        <FormatCoin value={balance} />
    )
}

export default BalanceLabel;