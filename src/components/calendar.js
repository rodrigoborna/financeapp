import  React, {useState} from 'react';
import { Text, View, StyleSheet } from 'react-native';
import DatePicker from "react-native-datepicker";


export default function App() {

  const [date, setDate] = useState(new Date())

  return (
      <View style={styles.container} >
        <DatePicker style={styles.calendar}
           format='YYYY/MM/DD'
           style={styles.dateComponent}
           mode='date'
          date={date}
            onDateChange={setDate}
            iconSource=''
            //  locale="tr" 
            // onSelect={(date) => console.log(date) }
            // isHideOnSelect={true}
            // initialDate={new Date()}
            // language={require('pt-br')}
        />
      </View>
  );
}

const styles = StyleSheet.create ({
    dateComponent: {
      width: 350
    },

    container: {
      justifyContent: 'center',
      flex: 1,
      alignContent: 'center'
    },

    calendar: {

    }
})