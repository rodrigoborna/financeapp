import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import HomeScreen from '../screens/home';
import ProfileScreen from '../screens/profile'
import DashboardScreen from '../screens/dashboard';
import ListMovementScreen from '../screens/dashboard/ListMovement';
import SidebarNav from './SidebarNav';



const Drawer = createDrawerNavigator();

export default function DrawerNavigation() {
    return (

<Drawer.Navigator drawerContent={props =><SidebarNav {...props}/>} >
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="DashBoard" component={DashboardScreen} />
        <Drawer.Screen name="Movimento" component={ListMovementScreen} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
</Drawer.Navigator>

 );
}

