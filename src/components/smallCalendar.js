import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Modal, FlatList } from "react-native";
import { format, startOfMonth, endOfMonth, isExists } from 'date-fns';
import { AntDesign } from '@expo/vector-icons';

import { colors } from '../constants/colors';

const months = [
    {
        lastDay: '31',
        number: '01',
        name: 'Janeiro',
        shortName: 'JAN',
    },
    {
        lastDay: '28',
        number: '02',
        name: 'Fevereiro',
        shortName: 'FEV',
    },
    {
        lastDay: '31',
        number: '03',
        name: 'Março',
        shortName: 'MAR',
    },
    {
        lastDay: '30',
        number: '04',
        name: 'Abril',
        shortName: 'ABR',
    },
    {
        lastDay: '31',
        number: '05',
        name: 'Maio',
        shortName: 'MAI',
    },
    {
        lastDay: '30',
        number: '06',
        name: 'Junho',
        shortName: 'JUN',
    },
    {
        lastDay: '31',
        number: '07',
        name: 'Julho',
        shortName: 'JUL',
    },
    {
        lastDay: '31',
        number: '08',
        name: 'Agosto',
        shortName: 'AGO',
    },
    {
        lastDay: '30',
        number: '09',
        name: 'Setembro',
        shortName: 'SET',
    },
    {
        lastDay: '31',
        number: '10',
        name: 'Outubro',
        shortName: 'OUT',
    },
    {
        lastDay: '30',
        number: '11',
        name: 'Novembro',
        shortName: 'NOV',
    },
    {
        lastDay: '31',
        number: '12',
        name: 'Dezembro',
        shortName: 'DEZ',
    },
];

const SmallCalendar = (props) => {

    const date = new Date();
    const [lastDay, setLastDay] = useState();
    const [month, setMonth] = useState();
    const [year, setYear] = useState(parseInt(format(date, 'Y')));
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();

    useEffect(() => {
        async function setInterval() {
            setStartDate(year + '/' + month + '/' + 1);
            setEndDate(year + '/' + month + '/' + lastDay);
        }
        setInterval();
    }, [year, month])

    const renderItem = ({ item }) => (
        <TouchableOpacity
            style={
                month === item.number
                    ? styles.monthDescriptionSelected
                    : styles.monthDescription
            }

            onPress={() => {
                (item.number == 2 && isExists(year, 1, 29) ? setLastDay('29') : setLastDay(item.lastDay));
                setMonth(item.number);
            }}
        >

            <Text
                style={
                    month === item.number
                        ? styles.fontSelected
                        : styles.font
                }
            >
                {item.shortName}
            </Text>

        </TouchableOpacity>
    )

    return (

        <Modal
            animationType="slide"
            transparent={true}
            visible={props.visible}
        >

            <View style={styles.centeredView}>

                <View style={styles.smallCalendar}>

                    <View style={styles.tittle}>

                        <TouchableOpacity
                            style={styles.arrowLeft}
                            onPress={() => { setYear(year - 1) }}
                        >
                            <AntDesign name="left" size={25} color={colors.four} />
                        </TouchableOpacity>

                        <View style={styles.description}>
                            <Text style={{ color: colors.four, fontSize: 25 }}>
                                {year}
                            </Text>
                        </View>

                        <TouchableOpacity
                            style={styles.arrowRight}
                            onPress={() => { setYear(year + 1) }}
                        >
                            <AntDesign name="right" size={25} color={colors.four} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.months}>

                        <FlatList
                            data={months}
                            renderItem={renderItem}
                            numColumns={3}
                            keyExtractor={(item) => item.number}
                        // extraData={selectedItem}
                        />

                    </View>
                    <View style={styles.buttonCalendar}>
                        <View style={styles.buttonCancel}>

                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => {
                                    props.startDate(format(startOfMonth(date), 'yyyy/MM/dd'))
                                        , props.endDate(format(endOfMonth(date), 'yyyy/MM/dd'))
                                        , props.onClose(false)
                                }}
                            >
                                <Text>Data Atual</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.buttonConfirm}>

                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => {
                                    props.startDate(startDate)
                                        , props.endDate(endDate)
                                        , props.onClose(false)
                                }}
                            >
                                <Text>Confirmar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create(
    {
        smallCalendar: {
            backgroundColor: colors.tree,
            flex: 1,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            borderWidth: 7,
            borderColor: colors.tree,
            elevation: 3,
        },

        tittle: {
            // backgroundColor: 'yellow',
            flex: 0.2,
            flexDirection: 'row'
        },

        arrowLeft: {
            // backgroundColor: 'green',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center'
        },

        description: {
            // backgroundColor: 'blue',
            flex: 0.6,
            alignItems: 'center',
            justifyContent: 'center'
        },

        arrowRight: {
            // backgroundColor: 'red',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center'
        },

        months: {
            // backgroundColor: 'pink',
            flex: 0.6,
            flexDirection: 'row',

        },

        monthDescription: {
            backgroundColor: colors.one,
            flex: 2,
            borderColor: colors.tree,
            borderWidth: 1,
            padding: 7,
            alignItems: 'center',
            borderRadius: 12,
            borderWidth: 2,
            margin: 2,
        },

        monthDescriptionSelected: {
            backgroundColor: colors.tree,
            flex: 2,
            borderColor: colors.four,
            borderWidth: 1,
            padding: 7,
            alignItems: 'center',
            borderRadius: 12,
            borderWidth: 2,
            margin: 2,
        },

        font: {
            color: '#000000',
        },

        fontSelected: {
            color: colors.one,
        },

        buttonCalendar: {
            flex: 0.2,
            flexDirection: 'row',

        },

        buttonCancel: {
            flex: 0.5,
            alignItems: 'center',
            justifyContent: 'center'

        },

        buttonConfirm: {
            flex: 0.5,
            alignItems: 'center',
            justifyContent: 'center'
        },

        button: {
            backgroundColor: colors.two,
            minWidth: '80%',
            minHeight: '60%',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 12,

        },

        centeredView: {
            marginTop: '30%',
            margin: 20,
            // backgroundColor: "white",
            borderRadius: 20,
            height: '48%',
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5
        },

    }
)

export default SmallCalendar;