import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../../src/screens/login';
import RegisterScreen from '../../src/screens/register';
import ProfileScreen from '../../src/screens/profile'
import DrawerNavigation from '../../src/components/DrawerNavigation';
import ListMovementScreen from '../../src/screens/dashboard/ListMovement';
import DashboardScreen from '../../src/screens/dashboard';
import SmallCalendarScreen from '../components/smallCalendar'

const Stack = createStackNavigator();

export default function BaseNavigation() {
    return (

  <Stack.Navigator initialRouteName="LoginScreen"  screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={LoginScreen}/>
      <Stack.Screen name="Cadastrar" component={RegisterScreen}/>
      <Stack.Screen name="Home" component={DrawerNavigation}/>
      <Stack.Screen name="listMovement" component={ListMovementScreen}/>
      <Stack.Screen name="Dashboard" component={DashboardScreen}/>
      <Stack.Screen name="Profile" component={ProfileScreen}/>
      <Stack.Screen name="SmallCalendar" component={SmallCalendarScreen}/>
    </Stack.Navigator>

 );
}