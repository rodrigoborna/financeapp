import { setStatusBarTranslucent } from 'expo-status-bar';
import Firebase from '../config/firebase';

import { validateEmail } from './functions';

export function teste() {

    return (true);
}

export function logoutFB() {
    return Firebase.auth().signOut()
        .then(() => {
            return true;
        })
        .catch((error) => {
            console.log(error.code + ": " + error.message);
            return (error);
        });

    //console.log("status:" + status);
}

export function authStateFB({ navigation }) {
    return Firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            navigation.navigate("Home");
        } else {
            console.log('usuário não logado');
        }
    });
}

export async function resetPassFB(email) {

    const validateEmailReturn = validateEmail(email);

    if (!validateEmailReturn) {
        console.log("Formato de Email Incorreto");
        return false;
    } else {
        return await Firebase.auth().sendPasswordResetEmail(email).then(function () {
            console.log('email enviado para: ' + email);
            return true;
        }).catch(function (error) {
            console.log(error.code + ': ' + error.message);
            return (error.code);
        });
    }
}

export async function authFB(email, pass) {

    const validateEmailReturn = validateEmail(email);

    if (!validateEmailReturn) {
        console.log("Formato de Email Incorreto");
        return false;
    } else {
        return await Firebase.auth().signInWithEmailAndPassword(email, pass)
            .then((user) => {
                console.log("Usuário Autenticado");
                return true;
            })
            .catch((error) => {
                return error.code;
            });
    }
}

export async function registerFB(email, pass, name) {

    const validateEmailReturn = validateEmail(email);

    if (!validateEmailReturn) {
        console.log("Formato de Email Incorreto");
        return false;
    } else {
        return await Firebase.auth().createUserWithEmailAndPassword(email, pass)
            .then((user) => {
                updateProfileFB({ displayName: name })
                return true;
            })
            .catch((error) => {
                return error.code;
            });
    }

}

export function userFB() {
    var user = Firebase.auth().currentUser;
    return user;
}

export async function updateProfileFB(dados) {
    var user = Firebase.auth().currentUser;
 
    user.updateProfile(
        dados
    ).then(function () {
        return true;
        
    }).catch(function (error) {
        return error.code;
    })
}

export function changePassword(newPass) {
    var user = Firebase.auth().currentUser;
    user.updatePassword(newPass).then(function () {
        return true;
    }).catch(function (error) {
        return error.code;
    });
}

export function changeEmail(newEmail) {
    var auth = Firebase.auth();

    auth.sendPasswordResetEmail(newEmail).then(function () {
        return true;
    }).catch(function (error) {
        return error.code
    });
}

export async function uploadFB(imageName,uri){
    
  return await Firebase
        .storage()
        .ref(imageName)
        .put(uri)
        .then((snapshot) => {
            //You can check the image is now uploaded in the storage bucket
            console.log(`${imageName} has been successfully uploaded.`);
        })
        .catch((e) => console.log('uploading image error => ', e));

}

export async function getUrlImage(imageName){

    return await Firebase
    .storage()
    .ref('/' + imageName)
    .getDownloadURL()
    .then((url) => {
    //from url you can fetched the uploaded image easily
    console.log("get",url)
    return url;
  })
  .catch((e) => console.log('getting downloadURL of image error => ', e));
}