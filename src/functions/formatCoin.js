import React from 'react'

const FormatCoin = (props) => {
    return (
        <>{('R$ ' + (parseFloat(props.value)).toFixed(2).replace('.',',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))}</>
    )
}

export default FormatCoin;