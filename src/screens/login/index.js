import React, { useState, useEffect } from 'react';
import {
    View,
    KeyboardAvoidingView,
    Image,
    TextInput,
    Text,
    TouchableOpacity,
    StyleSheet,
    Animated,
    Keyboard,
    Modal
} from 'react-native';

import styles from './style';

import { authStateFB, resetPassFB, authFB } from '../../functions/FBFunctions'
import FirebaseErrors from '../../config/firebaseErrors';


export default function Login({ navigation }) {

    const [modalVisible, setModalVisible] = useState(false);

    const [offset] = useState(new Animated.ValueXY({ x: 0, y: 95 }));
    const [opacity] = useState(new Animated.Value(0));
    const [logo] = useState(new Animated.ValueXY({ x: 430, y: 430 }));

    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');
    const [fberror, setFbError] = useState('');
    let validate = true;

    async function auth(email, pass) {
        const result = await authFB(email, pass);

        if (!email || !pass) {
            setFbError("Favor informar seus dados de acesso.");
            setTimeout(function () { setFbError('') }, 5000);
            validate = false;

        }

        if (valdiate == true) {
            if (!result) {
                console.log("formato de email errado");
                setFbError("Formato de Email Incorreto.");
                setTimeout(function () { setFbError('') }, 5000);

            } else if (result == true) {
                console.log("Logando");
            } else {
                console.log("problema com autenticação: " + result);
                setFbError(FirebaseErrors[result]);
                setTimeout(function () { setFbError('') }, 5000);
            }
        }
    }

    async function resetPass(user) {
        const result = await resetPassFB(user);
        console.log("retorno: " + result);
        if (!result) {
            setFbError("Formato de email inválido");
            setTimeout(function () { setFbError('') }, 5000);

        } else if (result == true) {
            setFbError("Email para redefinir a senha enviado.");
            setTimeout(function () { setFbError('') }, 5000);
            setModalVisible(false)

        } else {
            setFbError(FirebaseErrors[result]);
            setTimeout(function () { setFbError('') }, 5000);

        }

    }


    useEffect(() => {

        authStateFB({ navigation });

        keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', keyboardDidShow)
        keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', keyboardDidHide)

        Animated.parallel([
            Animated.spring(offset.y, {
                toValue: 0,
                speed: 4,
                bounciness: 20,
                useNativeDriver: false
            }),
            Animated.timing(opacity, {
                toValue: 1,
                duration: 300,
                useNativeDriver: false
            })

        ]).start();

    }, []);

    function keyboardDidShow() {
        Animated.parallel([
            Animated.timing(logo.x, {
                toValue: 230,
                duration: 50,
                useNativeDriver: false
            }),
            Animated.timing(logo.y, {
                toValue: 230,
                duration: 50,
                useNativeDriver: false
            })
        ]).start();

    }


    function keyboardDidHide() {
        Animated.parallel([
            Animated.timing(logo.x, {
                toValue: 430,
                duration: 50,
                useNativeDriver: false
            }),
            Animated.timing(logo.y, {
                toValue: 430,
                duration: 50,
                useNativeDriver: false
            })
        ]).start();
    }


    return (


        <KeyboardAvoidingView style={styles.background}>

            <View style={styles.containerLogo}>
                <Animated.Image
                    style={{ width: logo.x, height: logo.y }}
                    source={require('../../assets/logo.png')} />
            </View>
            <Animated.View
                style={[
                    styles.container,
                    {
                        opacity: opacity,
                        transform: [
                            { translateY: offset.y }
                        ]
                    }
                ]}>
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    autoCorrect={false}
                    onChangeText={user => setUser(user)}
                    defaultValue={user}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Senha"
                    autoCorrect={false}
                    onChangeText={pass => setPass(pass)}
                    defaultValue={pass}
                    secureTextEntry={true}
                />
                <TouchableOpacity style={styles.btnSubmit} onPress={() => auth(user, pass)}>
                    <Text style={styles.txtSubmit}>Acessar</Text>
                </TouchableOpacity>
                <View style={styles.vwButtons}>
                    <TouchableOpacity style={styles.btnRegister} onPress={() => navigation.navigate("Cadastrar")}>
                        <Text style={styles.txtRegister}>Cadastrar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnReset} onPress={() => setModalVisible(true)}>
                        <Text style={styles.txtRegister}>Esqueci a senha</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.txtError}>{fberror}</Text>
            </Animated.View>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.txtModal}>Favor infomar o email cadastrado.</Text>
                        <TextInput
                            style={styles.inputModal}
                            placeholder="Email"
                            autoCorrect={false}
                            onChangeText={user => setUser(user)}
                            defaultValue={user}

                        />
                        <View style={styles.vwButtons}>
                            <TouchableOpacity style={styles.btnModalSubmit} onPress={() => resetPass(user)}>
                                <Text style={styles.txtRegister}>Enviar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btnModalCancel} onPress={() => setModalVisible(false)}>
                                <Text style={styles.txtRegister}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal>

        </KeyboardAvoidingView>


    );
}