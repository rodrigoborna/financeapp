import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors'

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.two
    },
    containerLogo: {
        flex: 1,
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },
    input: {
        backgroundColor: '#fff',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 15,
        borderRadius: 7,
        padding: 10
    },
    btnSubmit: {
        backgroundColor: '#35AAFF',
        width: '90%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7

    },
    txtSubmit: {
        color: '#fff',
        fontSize: 17

    },
    btnRegister: {
        marginTop: 10,
        marginRight: 50

    },
    btnReset: {
        marginTop: 10

    },
    txtRegister: {
        color: '#fff',
        fontSize: 14

    },
    txtError: {
        color: 'red',
        fontSize: 14,
        alignItems:'center',
        justifyContent:'center',
        marginTop:40

    },vwButtons:{
        flexDirection:"row"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10
      },
      modalView: {
        backgroundColor: colors.one,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: colors.two,
        shadowOffset: {
          width: 0,
          height: 2
        },shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      btnModalSubmit: {
        backgroundColor: '#3CB371',
        width: '40%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        marginRight:25
      },
      btnModalCancel: {
        backgroundColor: '#B22222',
        width: '40%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        marginBottom:20        
    },
    txtModal:{
        fontSize:16,
        marginBottom:10
    },
    inputModal: {
        backgroundColor: '#fff',
        width: 300,
        marginBottom: 15,
        color: '#222',
        fontSize: 15,
        borderRadius: 7,
        padding: 10
    },     

});

export default styles;