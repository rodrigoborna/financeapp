import React, { useState, useEffect } from 'react';
import {
    View,
    KeyboardAvoidingView,
    Image,
    TextInput,
    Text,
    TouchableOpacity,
    StyleSheet,
    Animated,
    Keyboard
} from 'react-native';

import styles from './style';

import FirebaseErrors from '../../config/firebaseErrors';
import { registerFB } from '../../functions/FBFunctions'


export default function Register({ navigation }) {

    const [offset] = useState(new Animated.ValueXY({ x: 0, y: 95 }));
    const [opacity] = useState(new Animated.Value(0));
    const [logo] = useState(new Animated.ValueXY({ x: 430, y: 430 }));
    const [user, setUser] = useState('');
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [pass2, setPass2] = useState('');
    const [registerError, setRegisterError] = useState('');


    useEffect(() => {
        keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', keyboardDidShow)
        keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', keyboardDidHide)

        Animated.parallel([
            Animated.spring(offset.y, {
                toValue: 0,
                speed: 4,
                bounciness: 20,
                useNativeDriver: false
            }),
            Animated.timing(opacity, {
                toValue: 1,
                duration: 300,
                useNativeDriver: false
            })

        ]).start();

    }, []);

    function keyboardDidShow() {
        Animated.parallel([
            Animated.timing(logo.x, {
                toValue: 200,
                duration: 50,
                useNativeDriver: false
            }),
            Animated.timing(logo.y, {
                toValue: 200,
                duration: 50,
                useNativeDriver: false
            })
        ]).start();

    }


    function keyboardDidHide() {
        Animated.parallel([
            Animated.timing(logo.x, {
                toValue: 430,
                duration: 50,
                useNativeDriver: false
            }),
            Animated.timing(logo.y, {
                toValue: 430,
                duration: 50,
                useNativeDriver: false
            })
        ]).start();
    }
    let validate = true;
    async function register(email, pass, pass2, user) {
        if (!email || !pass || !pass2) {
            setRegisterError("Favor informar todos seus dados.");
            setTimeout(function () { setRegisterError('') }, 5000);
            validate = false;

        } else if (pass != pass2) {
            setRegisterError("Senhas Informadas diferentes.");
            setTimeout(function () { setRegisterError('') }, 5000);
            validate = false;
        }

        if (validate == true) {
            const result = await registerFB(email, pass, user)
            if (!result) {
                console.log("Formato de email errado");
                setRegisterError("Formato de Email Incorreto.");
                setTimeout(function () { setRegisterError('') }, 5000);

            } else if (result == true) {
                console.log("Logando");
            } else {
                console.log("Problema com Criação: " + result);
                setRegisterError(FirebaseErrors[result]);
                setTimeout(function () { setRegisterError('') }, 5000);
            }
        }

    }

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.containerLogo}>
                <Animated.Image
                    style={{ width: logo.x, height: logo.y }}
                    source={require('../../assets/logo.png')} />
            </View>
            <Animated.View
                style={[
                    styles.container,
                    {
                        opacity: opacity,
                        transform: [
                            { translateY: offset.y }
                        ]
                    }
                ]}>
                <Text style={styles.txtError}>{registerError}</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Informe seu Nome"
                    autoCorrect={false}

                    onChangeText={user => setUser(user)}
                    defaultValue={user}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Informe seu Email"
                    autoCorrect={false}

                    onChangeText={email => setEmail(email)}
                    defaultValue={email}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Defina uma Senha"
                    autoCorrect={false}
                    onChangeText={pass => setPass(pass)}
                    defaultValue={pass}
                    secureTextEntry={true}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Redigite a Senha"
                    autoCorrect={false}
                    secureTextEntry={true}
                    onChangeText={pass2 => setPass2(pass2)}
                />
                <Text style={styles.txtPassInfo}>*Sua senha precisa ter no mínimo 6 caracteres</Text>
                <View style={styles.vwButtons}>
                    <TouchableOpacity style={styles.btnSubmit} onPress={() => register(email, pass, pass2,user)}>
                        <Text style={styles.txtSubmit}>Cadastrar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnCancel} onPress={() => navigation.navigate("Login")}>
                        <Text style={styles.txtCancel}>Cancelar</Text>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        </KeyboardAvoidingView>
    );
}