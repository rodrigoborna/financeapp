import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors'

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.two
    },
    containerLogo: {
        flex: 1,
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },
    vwButtons:{
        flexDirection:"row"
    },
    input: {
        backgroundColor: '#fff',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 15,
        borderRadius: 7,
        padding: 10
    },
    btnSubmit: {
        backgroundColor: '#3CB371',
        width: '40%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        marginRight:25
        

    },
    txtSubmit: {
        color: '#fff',
        fontSize: 17

    },
    btnCancel: {
        backgroundColor: '#B22222',
        width: '40%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        marginBottom:20        
    },
    txtCancel: {
        color: '#fff',
        fontSize: 17

    },
    txtPassInfo: {
        color: '#fff',
        fontSize: 14,
        marginBottom:10

    },
    txtError: {
        color: 'red',
        fontSize: 14,
        alignItems:'center',
        justifyContent:'center',
        marginBottom:10

    }

});

export default styles;