import { StyleSheet } from "react-native";

const styles = StyleSheet.create(
  {
    main: {
      backgroundColor: "#f7f7e8",
      flex: 1,
      height: 500
    },

    safeAreaView: {
      flex: 1,
      marginTop:20
    },

    scrollView: {
      flex: 1
    },

    menu: {
      // backgroundColor: "pink",
      alignItems: "flex-end",
      marginTop: 30,
      paddingRight: 10,
      flex: 0.05,
    },

    container: {
      // backgroundColor: "blue",
      flex: 0.9,
    },
  }
);

export default styles;
