import React from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import { EvilIcons } from "@expo/vector-icons";
import styles from "./styles";
import {colors} from '../../../constants/colors'


const cardImage =
  "https://cdn.iconscout.com/icon/free/png-512/mastercard-25-675722.png";

const CardCards = () => {
  const handleShowClosed = () => {
    alert("Faturas fechadas");
  };

  return (
    <View style={styles.cardCards}>
      <View style={styles.tittle}>
        <Text style={{ fontSize: 30, color:  colors.four }}>Cartões de credito</Text>
      </View>
      <View style={styles.container}>
        <View style={styles.invoice}>
          <View style={styles.open}>
            <TouchableOpacity style={styles.buttonOpen} onPress={()=>navigation.navigate('dashboard')}>
              <Text style={{ color: colors.tree }}>Faturas abertas</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.closed}>
            <TouchableOpacity style={styles.buttonClose} onPress={handleShowClosed}>
              <Text style={{ color: colors.four }}>Faturas fechadas</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bank}>
          <View style={styles.flag}>
            <Image source={{ uri: cardImage }} style={styles.image} />
          </View>
          <View style={styles.nameBankAndValue}>
            <View style={styles.nameBank}>
              <Text style={{ fontWeight: 'bold', color: colors.four }}>ITAU</Text>
              <Text style={{ color: colors.four }}>{` - Fechada em 29 out, 2020`}</Text>
            </View>
            <View style={styles.currentValue}>
              <Text style={{ color: colors.negative, marginTop: 5 }}>R$ 139,99</Text>
            </View>
          </View>
          <View style={styles.iconInclude}>
            <EvilIcons name="plus" size={35} color={colors.tree} />
          </View>
        </View>
        <View style={styles.totalTextAndValue}>
          <View style={styles.totalText}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: colors.four }}>TOTAL</Text>
          </View>
          <View style={styles.totalValue}>
            <Text style={{ fontSize: 20, color: colors.four }}>R$ 139,99</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CardCards;
