import { StyleSheet } from "react-native";
import {colors} from '../../../constants/colors'

const styles = StyleSheet.create(
  {
    cardCards: {
      backgroundColor: colors.two,
      margin: 10,
      flex: 1,
      borderRadius: 12,
      borderWidth: 5,
      borderColor: colors.two,
      elevation: 3
    },

    tittle: {
      // backgroundColor: 'skyblue',
      padding: 1,
      flex: 0.2,
      alignItems: 'center',
      marginBottom: 5
    },

    container: {
      // backgroundColor: 'steelblue',
      flex: 0.8,
    },

    invoice: {
      flexDirection: "row",
      // backgroundColor: 'orange',
      flex: 0.3,
      marginBottom: 5
    },

    open: {
      // backgroundColor: 'green',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0.5,
    },

    closed: {
      // backgroundColor: 'yellow',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0.5,
    },

    bank: {
      flexDirection: 'row',
      // backgroundColor: 'pink',
      flex: 0.3,
      marginBottom: 5
    },

    flag: {
      // backgroundColor: 'blue',
      flex: 0.2,
      alignItems: 'center',
      justifyContent: 'center',
    },

    nameBankAndValue: {
      flexDirection: 'column',
      // backgroundColor: 'red',
      flex: 0.6,
    },

    iconInclude: {
      // backgroundColor: 'purple',
      flex: 0.2,
      alignItems: 'center',
      justifyContent: 'center',
    },

    nameBank: {
      flexDirection: 'row',
      // backgroundColor: 'brown',
      flex: 0.5,
      alignItems: 'center',
      justifyContent: 'center',
    },

    currentValue: {
      // backgroundColor: 'grey',
      flex: 0.5,
    },

    totalTextAndValue: {
      flexDirection: 'row',
      // backgroundColor: 'pink',
      flex: 0.3,
      marginBottom: 5
    },

    totalText: {
      // backgroundColor: 'green',
      flex: 0.5,
      alignItems: 'flex-start',
      justifyContent: 'center',
    },

    totalValue: {
      // backgroundColor: 'yellow',
      flex: 0.5,
      alignItems: 'center',
      justifyContent: 'center',
    },

    buttonOpen: {
      backgroundColor: colors.four,
      width: '80%',
      padding: 10,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 3
    },

    buttonClose: {
      backgroundColor: colors.tree,
      width: '80%',
      padding: 10,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 3
    },

    image: {
      width: 40,
      height: 40,
    },
  }
);

export default styles;
