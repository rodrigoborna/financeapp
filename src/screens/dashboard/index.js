import React from "react";
import { SafeAreaView, ScrollView, View,TouchableOpacity } from "react-native";
import styles from "./styles";


import CardMain from "./CardMain";
import CardCards from "./CardCards";
import CardGraphics from "./CardGraphics";
import CardPlans from "./CardPlans";
import SideBarNavIcon from '../../components/SideBarNavIcon';

import {insertMoviment, insertUser} from '../../config/crud';

// insertUser (1, 'Rodrigo', '123456', 'rodrigo.nascimento@outlook.com')

// insertMoviment(1, 'Almoço', -23.9, '2020/12/08', '2020/12/08', 'Alimentação', 'Debito', null)

// insertMoviment(1, 'Café', -5.5, '2020/12/12', '2020/12/12', 'Alimentação', 'Dinheiro', null)

// insertMoviment(1, 'Gasolina', -150, '2020/12/17', '2020/12/17', 'Combustivel', 'Debito', null)

// insertMoviment(1, 'Pedagio', -17.8, '2020/12/17', '2020/12/17', 'Locomoção', 'Dinheiro', null)

// insertMoviment(1, 'aluguel', -480, '2020/12/31', '2020/12/31', 'Moradia', 'Debito', null)

// insertMoviment(1, 'Jogo PS5', -299, '2021/01/08', '2021/01/08', 'Lazer', 'Debito', null)

// insertMoviment(1, 'Remédio', -12, '2021/01/13', '2021/01/13', 'Saude', 'Dinheiro', null)

// insertMoviment(1, 'Energia', -90, '2021/01/21', '2021/01/21', 'Concessionaria', 'Debito', null)

// insertMoviment(1, 'Telefone', -120, '2021/01/28', '2021/01/28', 'Concessionaria', 'Debito', null)

// insertMoviment(1, 'Serv. Manutenção', 180, '2021/01/31', '2021/01/31', 'Serviço', 'Dinheiro', null)

// insertMoviment(1, 'Pagamento', 7000, '2021/01/31', '2021/01/31', 'Salario', 'Debito', null)

// insertMoviment(1, 'Passagem', -4.5, '2021/02/01', '2021/02/01', 'Alimentação', 'Dinheiro', null)

// insertMoviment(1, 'HeadSet 01/05', -100, '2021/02/08', '2021/03/09', 'Eletronicos', 'Santander', null)

// insertMoviment(1, 'HeadSet 02/05', -100, '2021/02/08', '2021/04/09', 'Eletronicos', 'Santander', null)

// insertMoviment(1, 'HeadSet 03/05', -100, '2021/02/08', '2021/05/09', 'Eletronicos', 'Santander', null)

// insertMoviment(1, 'HeadSet 04/05', -100, '2021/02/08', '2021/06/09', 'Eletronicos', 'Santander', null)

// insertMoviment(1, 'HeadSet 05/05', -100, '2021/02/08', '2021/07/09', 'Eletronicos', 'Santander', null)

// insertMoviment(1, 'Hospedagem 01/02', -80, '2020/08/21', '2020/09/09', 'Viagem', 'Santander', null)

// insertMoviment(1, 'Hospedagem 02/02', -80, '2020/08/21', '2020/10/09', 'Viagem', 'Santander', null)

// insertMoviment(1, 'Aereas 01/03', -4.5, '2020/08/21', '2020/09/09', 'Viagem', 'Santander', null)

// insertMoviment(1, 'Aereas 02/03', -4.5, '2020/08/21', '2020/10/09', 'Viagem', 'Santander', null)

// insertMoviment(1, 'Aereas 03/03', -4.5, '2020/08/21', '2020/11/09', 'Viagem', 'Santander', null)

export default function Dashboard({ navigation }) {

  return (

    <View style={styles.main}> 
      <SafeAreaView style={styles.safeAreaView}>
        <SideBarNavIcon style={styles.menu} navigation={navigation} />
        <ScrollView style={styles.scrollView}>

          <View style={styles.container}>
            <CardMain navigation={navigation} />
            <CardCards />
            <CardGraphics />
            <CardPlans />
          </View>

        </ScrollView>

      </SafeAreaView>

    </View>
  );
}


