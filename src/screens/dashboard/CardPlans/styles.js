import { StyleSheet } from "react-native";
import {colors} from '../../../constants/colors'

const styles = StyleSheet.create(
  {
  cardPlans: {
    backgroundColor: colors.two,
    margin: 10,
    borderRadius: 12,
    borderWidth: 5,
    borderColor: colors.two,
    flex: 1,
    elevation: 3
  },

  tittle: {
    // backgroundColor: 'skyblue',
    padding: 10,
    flex: 0.2,
    alignItems: 'center',
    marginBottom: 5
  },

  container: {
    // backgroundColor: 'steelblue',
    flex: 0.8,
    marginBottom: 5
  },

  iconList: {
    // backgroundColor: 'red',
    flex: 0.35,
    alignItems: 'center',
  },

  lineTwo: {
    // backgroundColor: 'blue',
    flex: 0.35,
    textAlign: 'center',
    marginBottom: 5
  },

  lineThree: {
    // backgroundColor: 'pink',
    flex: 0.10,
    marginBottom: 5
  },

  lineFour: {
    // backgroundColor: 'green',
    flex: 0.20,
    alignItems: 'center',
    marginBottom: 5
  },

  
  button: {
    width: "80%",
    paddingVertical: 20,
    backgroundColor: colors.tree,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
}
);

export default styles;
