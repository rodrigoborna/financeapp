import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import styles from "./styles";
import {colors} from '../../../constants/colors'


const CardPlans = () => {

  return (

    <View style={styles.cardPlans}>
      <View style={styles.tittle}>
        <Text style={{ fontSize: 30, color: colors.four }}>Planejamento Mensal</Text>
      </View>
      <View style={styles.container}>

        <View style={styles.iconList}>
          <FontAwesome name="list-alt" size={45} color={colors.tree} />
        </View>
        <View style={styles.lineTwo}>
          <Text style={{fontSize: 20, textAlign: 'center', color: colors.four}}>
            Ops! Voce nao tem um planejamento definido para esse mes
        </Text>
        </View>
        <View style={styles.lineThree}>
          <Text style={{color: colors.four, textAlign: 'center'}}>
            Melhore seu controle financeiro agora!
        </Text>
        </View>
        <View style={styles.lineFour}>
          <TouchableOpacity style={styles.button}>
            <Text style={{color: colors.four}}>DEFINIR MEU PLANEJAMENTO</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View >
  );
};

export default CardPlans;
