import React from "react";
import { Text, View } from "react-native";
import { PieChart } from "react-native-svg-charts";
import styles from "./styles";
import {colors} from '../../../constants/colors'


const CardGraphics = () => {

  const randomColor = () =>

    ("#" + ((Math.random() * 0xffffff) << 0).toString(16) + "000000").slice(0, 7);

  const data = [
    {
      id: 0,
      value: 720,
      description: "Alimentação",
      color: randomColor(),
    },
    {
      id: 1,
      value: 310,
      description: "Carro",
      color: randomColor(),
    },
    {
      id: 2,
      value: 250,
      description: "Investimento",
      color: randomColor(),
    },
    {
      id: 3,
      value: 321,
      description: "Outros",
      color: randomColor(),
    },
    {
      id: 4,
      value: 121,
      description: "Bebidas",
      color: randomColor(),
    },
  ];

  const pieData = data.map((item, index) => ({
    value: item.value,
    svg: {
      fill: item.color,
    },
    key: `pie-${index}`,
  }));

  return (

    <View style={styles.PieChart}>

      <View style={styles.tittle}>
        <Text style={{ fontSize: 30, color: colors.four }}> Despesa por categorias</Text>
      </View>

      <View style={styles.container}>

        <View style={styles.graphic}>
          <PieChart style={{ height: 120 }} data={pieData} />
        </View>

        <View style={styles.listExpense}>

          {data.map((item, index) => (

            <View style={styles.lineDetail} key={`${index}`}>

              <View style={styles.ballColor}>
                <View style={[styles.circle, { backgroundColor: item.color }]} />
              </View>
              <View style={styles.expense}>
                <Text style={{color: colors.four}}>{item.description}</Text>
              </View>
              <View style={styles.value}>
                <Text style={{color: colors.four}}>{`R$ ${item.value}`}</Text>
              </View>
            </View>
          ))}

        </View>

      </View>
    </View>
  );
};

export default CardGraphics;
