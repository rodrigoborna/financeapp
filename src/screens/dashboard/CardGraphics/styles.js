import { StyleSheet } from "react-native";
import {colors} from '../../../constants/colors'

const styles = StyleSheet.create(
  {

    PieChart: {
      backgroundColor: colors.two,
      margin: 10,
      flex: 1,
      borderRadius: 12,
      borderWidth: 5,
      borderColor: colors.two,
      elevation: 3
    },

    tittle: {
      // backgroundColor: 'yellow',
      flex: 0.33,
      marginBottom: 5
    },

    container: {
      flexDirection: "row",
      // backgroundColor: "#ADD8E6",
      flex: 0.77,
      marginBottom: 5
    },

    graphic: {
      // backgroundColor: "purple",
      flex: 0.4,
    },

    listExpense: {
      // backgroundColor: "pink",
      flex: 0.6,
    },

    lineDetail: {
      flexDirection: "row",
      // borderBottomWidth: 1,
      justifyContent: "space-between",
      flex: 1,
    },

    circle: {
      // backgroundColor: "blue",
      width: 15,
      height: 15,
      borderRadius: 10,
    },

    ballColor: {
      // backgroundColor: "grey",
      flex: 0.15,
      alignItems: 'center',
      justifyContent: 'center'
    },

    expense: {
      // backgroundColor: "brown",
      flex: 0.60,
      alignContent:'flex-start'
    },

    value: {
      // backgroundColor: "green",
      flex: 0.25,
    },
  }
);

export default styles;
