import React, { useState, useEffect, useLayoutEffect } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { Entypo, FontAwesome5, MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";

import styles from "./styles";
import { colors } from '../../../constants/colors';
import SmallCalendar from '../../../components/smallCalendar';
import FormatCoin from '../../../functions/formatCoin';


import BalanceLabel from '../../../components/label/BalanceLabel';
import ExpenseLabel from '../../../components/label/ExpenseLabel';
import IncomeLabel from '../../../components/label/IncomeLabel';
import { format, startOfMonth, endOfMonth } from "date-fns";

export default function CardMain({ navigation }) {

  console.log('CardMain')

  const date = new Date();
  const [startDate, setStardDate] = useState(format(startOfMonth(date), 'yyyy/MM/dd'));
  const [endDate, setEndDate] = useState(format(endOfMonth(date), 'yyyy/MM/dd'));

  const [showValues, setShowValues] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
   console.log("Depois do render")
}, [])

useLayoutEffect(() => {
  console.log("Antes do render")
}, [])

  return (

    <View style={styles.cardMain} >

      <View style={styles.tittle}>
        <Text style={{ fontSize: 30, color: colors.four }}> Resumo Carteira </Text>
      </View>

      <View style={styles.photo}>
        <FontAwesome5 name="user-circle" size={80} color={colors.tree} />
      </View>

      <View style={styles.container}>
        <View style={styles.lineOne}>

          <View style={styles.userCircle} />

          <TouchableOpacity
            style={styles.calendar}
            onPress={() => setModalVisible(true)}
          >

            <Text style={{ color: colors.four }}>
              {
                format(new Date(startDate), 'MMM Y')
              }
            </Text>

            <SmallCalendar
              visible={modalVisible}
              onClose={setModalVisible}
              startDate={setStardDate}
              endDate={setEndDate}
            />

            <MaterialIcons name="keyboard-arrow-down" size={24} color={colors.four} />
          </TouchableOpacity>

          <View style={styles.bell}>
            <FontAwesome5 name="bell" size={22} color={colors.tree} />
          </View>

          <View style={styles.dotsTree}>
            <Entypo name="dots-three-vertical" size={22} color={colors.tree} />
          </View>
        </View>

        <View style={styles.lineTwo}>
          <Text style={{ color: colors.four }}>Saldo em conta</Text>
        </View>

        <View style={styles.lineThree}>
          <Text style={{ fontSize: 25, color: colors.four }}>
            {
              showValues
                ? <BalanceLabel />
                : "   "
            }
          </Text>
        </View>

        <View style={styles.lineFour}>
          <Entypo name={
            showValues
              ? "eye"
              : "eye-with-line"
          }
            size={22}
            color={colors.tree}
            onPress={() => { setShowValues(!showValues) }}
          />
        </View>

        <View style={styles.lineFive} >

          <TouchableOpacity
            style={styles.Expense}
            onPress={() => navigation.navigate("listMovement")}>

            <View style={styles.iconExpense}>
              <MaterialCommunityIcons name="arrow-down-circle" size={40} color={colors.negative} />
            </View>

            <View style={styles.textAndValueExpense}>

              <View style={styles.textExpense}>
                <Text style={{ color: colors.four }}>Despesas</Text>
              </View>

              <View style={styles.valueExpense}>
                <Text style={{ fontSize: 20, color: colors.negative, justifyContent: "flex-start" }}>
                  {
                    showValues
                      ? <ExpenseLabel startMonth={startDate} endMonth={endDate} />
                      : "   "
                  }
                </Text>
              </View>

            </View>
          </TouchableOpacity>

          <View style={styles.Income}>

            <View style={styles.iconIncome}>
              <MaterialCommunityIcons name="arrow-up-circle" size={40} color={colors.positive} />
            </View>

            <View style={styles.textAndValueIncome}>

              <View style={styles.textIncome}>
                <Text style={{ color: colors.four }}>Receitas</Text>
              </View>

              <View style={styles.valueIncome}>
                <Text style={{ fontSize: 20, color: colors.positive }}>
                  {
                    showValues
                      ? <IncomeLabel startMonth={startDate} endMonth={endDate} />
                      : "   "
                  }
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

