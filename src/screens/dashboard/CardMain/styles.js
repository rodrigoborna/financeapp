import { StyleSheet } from "react-native";
import {colors} from '../../../constants/colors'

const styles = StyleSheet.create(
  {
    cardMain: {
      backgroundColor: colors.two,
      margin: 10,
      borderRadius: 12,
      borderWidth: 5,
      borderColor: '#c7cfb7',
      flex: 1,
      elevation: 3
    },

    tittle: {
      // backgroundColor: 'skyblue',
      padding: 10,
      flex: 0.2,
      alignItems: 'center',
      marginBottom: 5
    },

    photo: {
      position: 'absolute',
      //backgroundColor: 'yellow',
      width: 80,
      height: 80,
      top: '25%',
      left: '02%',
      borderRadius: 100,
      overflow: "hidden",
      elevation: 1
    },

    container: {
      // backgroundColor: 'steelblue',
      flex: 0.8,
      marginBottom: 5
    },

    lineOne: {
      flexDirection: 'row',
      flex: 0.2,
      marginBottom: 5,
    },
    userCircle: {
      // backgroundColor: 'red',
      flex: 0.25
    },

    calendar: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      // backgroundColor: 'grey',
      flex: 0.5
    },

    bell: {
      // backgroundColor: 'yellow',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0.15
    },

    dotsTree: {
      // backgroundColor: 'orange',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0.1
    },


    lineTwo: {
      // backgroundColor: 'brown',
      flex: 0.15,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 5,
    },

    lineThree: {
      // backgroundColor: 'pink',
      flex: 0.2,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 5,
    },

    lineFour: {
      // backgroundColor: 'purple',
      flex: 0.15,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 5,
    },

    lineFive: {
      flexDirection: 'row',
      flex: 0.3,
      marginBottom: 5,
    },

    Expense: {
      flexDirection: 'row',
      // height: '100%',
      // width: '50%'
      flex: 1
    },

    iconExpense: {
      // backgroundColor: 'blue',
      flex: 0.3,
      alignItems: 'center',
      justifyContent: 'center',
    },

    textAndValueExpense: {
      flexDirection: 'column',
      flex: 0.7
    },

    textExpense: {
      // backgroundColor: 'yellow',
      flex: 0.4,
      justifyContent: 'center',
    },

    valueExpense: {
      // backgroundColor: 'red',
      flex: 0.6
    },

    Income: {
      flexDirection: 'row',
      flex: 1
    },

    iconIncome: {
      // backgroundColor: 'blue',
      flex: 0.3,
      alignItems: 'center',
      justifyContent: 'center',
    },

    textAndValueIncome: {
      flexDirection: 'column',
      flex: 0.7
    },

    textIncome: {
      // backgroundColor: 'yellow',
      flex: 0.4,
      justifyContent: 'center',
    },

    valueIncome: {
      // backgroundColor: 'red',
      flex: 0.6
    },
  }
);

export default styles;
