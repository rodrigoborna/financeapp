import { StyleSheet } from "react-native";
import {colors} from '../../../constants/colors'
const styles = StyleSheet.create(
    {
        cardListExpense: {
            backgroundColor: colors.two,
            margin: 10,
            flex: 1,
            borderRadius: 12,
            borderWidth: 5,
            borderColor: colors.two,
            marginTop: '10%',
            elevation: 3
        },

        container: {
            backgroundColor: colors.two,
            flex: 1,
        },

        menu: {
            // backgroundColor: 'blue',
            flex: 0.1,
            flexDirection: 'row'
        },

        tittle: {
            // backgroundColor: 'red',
            flex: 0.30,
            alignItems: 'center',
            justifyContent: 'center'
        },

        button: {
            // backgroundColor: 'blue',
            flex: 0.05,
            alignItems: 'center',
            justifyContent: 'center'
        },

        calendar: {
            // backgroundColor: 'brown',
            flex: 0.25,
            alignItems: 'flex-end',
            justifyContent: 'center',
            paddingRight: '3%'
        },

        search: {
            // backgroundColor: 'orange',
            flex: 0.20,
            alignItems: 'center',
            justifyContent: 'center'
        },

        dotsThree: {
            // backgroundColor: 'green',
            flex: 0.20,
            alignItems: 'center',
            justifyContent: 'center'
        },

        monthYear: {
            // backgroundColor: 'yellow',
            flex: 0.07,
            flexDirection: 'row'
        },

        left: {
            // backgroundColor: 'red',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center'
        },

        description: {
            // backgroundColor: 'white',
            flex: 0.6,
            alignItems: 'center',
            justifyContent: 'center'
        },

        right: {
            // backgroundColor: 'yellow',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center'
        },


        totals: {
            // backgroundColor: 'grey',
            flex: 0.10,
            flexDirection: 'row'
        },

        Pending: {
            // backgroundColor: 'pink',
            flex: 0.5,
            flexDirection: 'row'
        },

        iconPending: {
            // backgroundColor: 'blue',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 5
        },

        totalPending: {
            // backgroundColor: 'black',
            flex: 0.8,
            flexDirection: 'column'
        },

        descriptionPending: {
            // backgroundColor: 'yellow',
            flex: 0.35,
            alignItems: 'flex-start',
            justifyContent: 'center',
            marginLeft: 5
        },

        valuePending: {
            // backgroundColor: 'green',
            flex: 0.65,
            alignItems: 'flex-start',
            justifyContent: 'center',
            marginLeft: 5
        },

        Paid: {
            // backgroundColor: 'orange',
            flex: 0.5,
            flexDirection: 'row'
        },

        iconPaid: {
            // backgroundColor: 'blue',
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 5
        },

        totalPaid: {
            // backgroundColor: 'black',
            flex: 0.8,
            flexDirection: 'column'
        },

        descriptionPaid: {
            // backgroundColor: 'yellow',
            flex: 0.35,
            alignItems: 'flex-start',
            justifyContent: 'center',
            marginLeft: 5
        },

        valuePaid: {
            // backgroundColor: 'green',
            flex: 0.65,
            alignItems: 'flex-start',
            justifyContent: 'center',
            marginLeft: 5
        },


        listExpense: {
            // backgroundColor: 'purple',
            flex: 0.73,
            justifyContent: "center",
            alignContent: "center"
        },

        line: {
            backgroundColor: colors.tree,
            flex: 1,
            flexDirection: 'column',
            maxHeight: '90%',
            maxWidth: '98%',
            margin: 4,
            borderColor: colors.tree,
            borderWidth: 7,
            borderRadius: 15,
        },

        weekday: {
            // backgroundColor: 'yellow',
            flex: 0.35,
        },

        movementData: {
            // backgroundColor: 'green',
            flex: 0.65,
            flexDirection: 'row'
        },

        iconMovement: {
            // backgroundColor: 'pink',
            flex: 0.2,
            textAlign: "center",
            justifyContent: "center"
        },

        descriptionData: {
            // backgroundColor: 'pink',
            flex: 0.6,
            flexDirection: 'column'
        },

        descriptionMovement: {
            // backgroundColor: 'brown',
            flex: 0.33,
            textAlign: 'left',
            justifyContent: "flex-end"
        },

        categoryWallet: {
            // backgroundColor: 'orange',
            flex: 0.33,
            flexDirection: 'row',
        },

        categoryMovement: {
            // backgroundColor: 'red',
            // flex: 0.5,
            textAlign: 'right',
            justifyContent: 'center',
            borderRightWidth: 1,
            borderRightColor: colors.two
        },

        walletMovement: {
            // backgroundColor: 'grey',
            // flex: 0.5,
            textAlign: 'left',
            justifyContent: 'center'
        },

        tag: {
            // backgroundColor: 'blue',
            flex: 0.33,
            textAlign: 'left',
            justifyContent: 'flex-start'
        },

        valueMovement: {
            // backgroundColor: 'purple',
            flex: 0.3,
            textAlign: 'right',
            justifyContent: 'center'
        }


    }
);

export default styles;