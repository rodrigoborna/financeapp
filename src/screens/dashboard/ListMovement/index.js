import React, { useState, useEffect } from 'react';
import { Text, View, FlatList } from 'react-native';
import firebase from '../../../config/firebase';
import styles from "./styles";
import {colors} from '../../../constants/colors'
import { AntDesign, Octicons, Entypo, MaterialIcons, Ionicons } from '@expo/vector-icons';
import moment from "moment";
import 'moment/locale/pt-br';


export default function listMovement() {


  const [value, setValue] = useState([]);


  function selectUser() {
    firebase.database().ref('users/1/movement').on('value', snapshot => {
      var main = [];
      snapshot.forEach((child) => {
        main.push({
          key: child.val(),
        })
      })
      setValue(main)
    });
  }

  useEffect(() => {
    selectUser()
  }, []);

  const [visible, setVisible] = useState(false);

  const renderItem = ({ item }) => (
    <View style={styles.line}>
      <View style={styles.weekday}>
        <Text style={{color: colors.four, fontSize: 15 }}> {moment(item.key.movementDate).format("dddd, D")} </Text>
      </View>
      <View style={styles.movementData}>
        <View style={styles.iconMovement}>
          <Text >
            <Text style={{color: colors.four, fontSize: 20 }}> {item.key.description.substr(0, 2).toUpperCase()} </Text>
          </Text>
        </View>
        <View style={styles.descriptionData}>
          <View style={styles.descriptionMovement}>
            <Text style={{color: colors.four, fontSize: 20 }}> {item.key.description} </Text>
          </View>
          <View style={styles.categoryWallet}>
            <View style={styles.categoryMovement}>
              <Text style={{color: colors.four, fontSize: 12 }}> {item.key.category} </Text>
            </View>
            <View style={styles.walletMovement}>
              <Text style={{color: colors.four, fontSize: 12 }}> {item.key.wallet} </Text>
            </View>
          </View >
          <View style={styles.tag}>
            <Text style={{color: colors.four, fontSize: 12 }}> tag descrição </Text>
          </View>
        </View>
        <View style={styles.valueMovement}>
          <Text style={{color: colors.negative, fontSize: 16 }}> {('R$ ' +(item.key.value).toFixed(2).replace('.',',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))} </Text>
        </View>
      </View>
    </View>
  );


  return (

    <View style={styles.cardListExpense}>

      <View style={styles.container}>

        <View style={styles.menu}>
          <View style={styles.tittle}>
            <Text style={{ color: colors.tree, fontSize: 15 }}>Despesas</Text>
          </View>
          <View style={styles.button}>
            <AntDesign name="down" size={15} color={colors.four} />
          </View>
          <View style={styles.calendar}>
            <Octicons name="calendar" size={25} color={colors.four} />
          </View>
          <View style={styles.search}>
            <AntDesign name="search1" size={25} color={colors.four} />
          </View>
          <View style={styles.dotsThree}>
            <Entypo name="dots-three-vertical" size={25} color={colors.four} />
          </View>
        </View>

        <View style={styles.monthYear}>
          <View style={styles.left}>
            <AntDesign name="left" size={15} color={colors.four} />
          </View>
          <View style={styles.description}>
            <Text style={{ color: colors.four, fontSize: 15 }}>Dezembro 2020</Text>
          </View>
          <View style={styles.right}>
            <AntDesign name="right" size={15} color={colors.four} />
          </View>
        </View>

        <View style={styles.totals}>
          <View style={styles.Pending}>
            <View style={styles.iconPending}>
              <MaterialIcons name="pending-actions" size={35} color={colors.four} />
            </View>
            <View style={styles.totalPending}>
              <View style={styles.descriptionPending}>
                <Text style={{ color: colors.four, fontSize: 15 }}>Total Pendente</Text>
              </View>
              <View style={styles.valuePending}>
                <Text style={{ color: colors.four, fontSize: 25 }}>R$ 0,00</Text>
              </View>
            </View>
          </View>
          <View style={styles.Paid}>
            <View style={styles.iconPaid}>
              <Ionicons name="wallet-outline" size={35} color={colors.four} />
            </View>
            <View style={styles.totalPaid}>
              <View style={styles.descriptionPaid}>
                <Text style={{ color: colors.four, fontSize: 15 }}>Total Pago</Text>
              </View>
              <View style={styles.valuePaid}>
                <Text style={{ color: colors.four, fontSize: 25 }}>R$ 0,00</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.listExpense}>

          <FlatList
            data={value}
            renderItem={renderItem}
            keyExtractor={(item) => item.key.description}
          />

        </View>


      </View>
    </View>
  );

}