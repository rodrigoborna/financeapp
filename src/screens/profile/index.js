import React, { useState, useEffect } from 'react';
import { Ionicons } from "@expo/vector-icons";
import {
    View,
    KeyboardAvoidingView,
    Image,
    TextInput,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import styles from './style';
import * as ImagePicker from 'expo-image-picker';

import FirebaseErrors from '../../config/firebaseErrors';
import { userFB, uploadFB, getUrlImage, updateProfileFB } from '../../functions/FBFunctions'



export default function Profile({ navigation }) {
  
    const [user,setUser] = useState(userFB());
    const [buttonText,setButtonText] = useState('Editar');
    const [editableText,setEditableText] = useState(false);
    const [showCancel,setShowCancel] = useState(false);
    const [name,setName] = useState(user.displayName);
    const [email,setEmail] = useState(user.email);
    const [phone,setPhone] = useState(user.phoneNumber);
    const [photoUrl,setPhotoUrl]=useState(user.photoURL);
 
 

    useEffect(()=>{
  
    });

     const openImagePicker = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          base64:true,
          aspect: [4, 3],
          quality: 1,
        });
    
        console.log("resultado",result.base64);
    
        if (!result.cancelled) {
          
          let blob = await fetch(result.uri).then(r=>r.blob())
          let imageName="profilePhoto-"+user.uid
          await uploadFB(imageName,blob);
          let url = await getUrlImage(imageName);
          await updateProfileFB({photoURL:url});
          setPhotoUrl(url);
        }
      };

      function editProfile(){
          setButtonText("Salvar");
          setEditableText(true);
          setShowCancel(true);
      }

      function cancelEditProfile(){
        setButtonText("Editar");
        setEditableText(false);
        setShowCancel(false);
    }

    return (
        <KeyboardAvoidingView style={styles.background}>
            
            <View style={styles.container}>
            <ScrollView>
                <View style={styles.header}>
                    <Ionicons name="person-circle-outline" size={55} color={'black'} />
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Perfil</Text>
                        <Text>Aqui pode gerenciar seus dados.</Text>
                    </View>
                </View>
                
                <View style={styles.profileImage}>
                    <View>
                        <TouchableOpacity style={styles.pictureAdd} onPress={() => openImagePicker()}>
                            <View >
                                <Ionicons name="pencil-sharp" size={30} color={'black'} />
                            </View>
                        </TouchableOpacity>
                        <Image source={!photoUrl ? require('../../assets/profile-pic2.jpg') :{uri:photoUrl}} style={styles.image} />
                    </View>

                </View>

                <View style={styles.content}>
                    <View>
                        <Text style={{fontWeight:'bold'}}>Nome</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Nome"
                        autoCorrect={false}
                        editable={editableText}
                        onChangeText={name => setName(name)}
                        defaultValue={name}/>

                        <Text style={{fontWeight:'bold'}}>Email</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Email"
                        autoCorrect={false}
                        editable={editableText}
                        onChangeText={email => setEmail(email)}
                        defaultValue={email}/>

                        <Text style={{fontWeight:'bold'}}>Telefone</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Telefone"
                        autoCorrect={false}
                        editable={editableText}
                        onChangeText={phone => setPhone(phone)}
                        defaultValue={phone}/>
                    </View>

                    <View style={styles.buttons}>
                        <TouchableOpacity style={styles.button} onPress={()=>editProfile()}>
                        <Text style={{color:'#fff', fontSize:16}}>{buttonText}</Text>
                        </TouchableOpacity>
                        {showCancel ?
                        (<TouchableOpacity style={styles.buttonCancel} onPress={()=>cancelEditProfile()}>
                        <Text style={{color:'#fff', fontSize:16}}>Cancelar</Text>
                        </TouchableOpacity>)
                        :null
                        }   
                    
                     </View>
                    
                </View>
                </ScrollView>   
            </View>
            
        </KeyboardAvoidingView>
    );
}