import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors'

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: colors.two,
        justifyContent:'center',
        alignItems:'center'
    },
    container:{
        flex:1,
        backgroundColor:"#fff",
        width:'95%',
        borderRadius:15,
        marginTop:45,
        marginBottom:25
    },
    profileImage:{
        flex:0.7,
        alignItems:'center',
        justifyContent:'center',
        marginTop:25
    },
    image:{
        width:140,
        height:140,
        borderRadius:100
    },
    pictureAdd:{
        borderRadius:80,
        backgroundColor:'brown',
        width: 40,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        zIndex:1,
        right:-1,
        bottom:1
    },
    header:{
        flex:0.2,
        flexDirection: 'row',
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        marginTop:10,
        marginLeft:15,
        alignItems:'center'
    },
    content:{
        flex:1,
        marginLeft:10,
        marginTop:25
    },
    input: {
        backgroundColor: '#DCDCDC',
        width: '98%',
        marginBottom: 15,
        color: 'black',
        fontSize: 15,
        borderRadius: 7,
        padding:10
    },
    buttons:{
        flex:1,
        marginBottom:20
    },
    button:{
        backgroundColor:'green',
        width:'98%',
        padding:10,
        borderRadius:18,
        alignItems:'center',
        justifyContent:'center',
        marginBottom:5
    },
    buttonCancel:{
        backgroundColor:'red',
        width:'98%',
        padding:10,
        borderRadius:18,
        alignItems:'center',
        justifyContent:'center'
    }


});

export default styles;