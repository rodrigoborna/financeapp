import {StyleSheet} from 'react-native';
import {colors} from '../../constants/colors'

const styles = StyleSheet.create({

    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    container2: {
      flex: 1,
      backgroundColor: colors.one
    },
    SafeArea:{
      marginTop:30,
      marginRight:15,
      alignItems:"flex-end",
      flex:0.2
      
    },
    text: {
      color: '#000',
      fontSize: 17,
      marginBottom:15
  
    },
    btnLogout: {
      backgroundColor: colors.tree,
      width: '90%',
      height: 45,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 7,
      marginBottom: 15
  
    },
    txtLogout: {
      color: '#fff',
      fontSize: 17
  
    },
    btnDashboard: {
      backgroundColor: colors.tree,
      width: '90%',
      height: 45,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 7,
      marginBottom: 15
  
    },
    txtDashboard: {
      color: '#fff',
      fontSize: 17

    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10
    },
    modalView: {
      margin: 20,
      backgroundColor: colors.one,
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
      
  })

  export default styles;