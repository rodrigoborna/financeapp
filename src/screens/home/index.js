import React from 'react';
import { useState, useEffect } from 'react';
import { Text, TouchableOpacity, View, SafeAreaView, Modal } from 'react-native';
import { FontAwesome5 } from "@expo/vector-icons";
import styles from './style'
import { format, startOfMonth } from 'date-fns';
import SideBarNavIcon from '../../components/SideBarNavIcon';
import { teste, logoutFB, userFB, updateProfileFB } from '../../functions/FBFunctions';

export default function Home({ navigation }) {
  const [modalVisible, setModalVisible] = useState(false);
  function testeLocal() {
    console.log(<FormatCoin value={100000} />)
  }

  function testeLocal2() {
    let month = [
      {
        id: '01',
        shortDescription: 'JAN',
        description: 'Janeiro'
      },
      {
        id: '02',
        shortDescription: 'FEV',
        description: 'Fevereiro'
      },
      {
        id: '03',
        shortDescription: 'MAR',
        description: 'Março'
      },
      {
        id: '04',
        shortDescription: 'ABR',
        description: 'Abril'
      },
      {
        id: '05',
        shortDescription: 'MAI',
        description: 'Maio'
      },
      {
        id: '06',
        shortDescription: 'JUN',
        description: 'Junho'
      },
      {
        id: '07',
        shortDescription: 'JUL',
        description: 'Julho'
      },
      {
        id: '08',
        shortDescription: 'AGO',
        description: 'Agosto'
      },
      {
        id: '09',
        shortDescription: 'SET',
        description: 'Setembro'
      },
      {
        id: '10',
        shortDescription: 'OUT',
        description: 'Outubro'
      },
      {
        id: '11',
        shortDescription: 'NOV',
        description: 'Novembro'
      },
      {
        id: '12',
        shortDescription: 'DEZ',
        description: 'Dezembro'
      },
    ]

    monthFilter = month.filter(month => month.id == "01")

    console.log("ID: " + monthFilter[0].id);
    console.log("Abrev: " + monthFilter[0].shortDescription);
    console.log("Deasc: " + monthFilter[0].description);
  }

  function userLocal() {
    let userData = userFB();
    //console.log(userData);
    console.log("UID: " + JSON.stringify(userData.uid));
    console.log("email: " + JSON.stringify(userData.email));
    console.log("displayName: " + JSON.stringify(userData.displayName));

  }

  async function sair() {
    const result = await logoutFB();

    if (result) {
      navigation.navigate("Login");
    } else {
      console.log("resultado: " + result);
    }

  }


  async function load() {
    const date = new Date();
    console.log('saida 1', Now('dd/MM/yyyy'))
    console.log('saida 2', OuterDate('12/12/2021', 'dd/MM/yyyy'))
    console.log('saida 3', StartMonth('12/12/2021', 'dd/MM/yyyy'))
    console.log('saida 4', format(startOfMonth(date), 'yyyy/MM/dd'))
  }

  return (
    <View style={styles.container2}>
      <SafeAreaView style={styles.SafeArea}>
        <SideBarNavIcon navigation={navigation} />
      </SafeAreaView>
      <View style={styles.container}>
        <Text style={styles.text}>Você esta Logado.</Text>
        <TouchableOpacity style={styles.btnLogout} onPress={() => sair()}>
          <Text style={styles.txtLogout}>Desconectar</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => navigation.navigate("DashBoard")}>
          <Text style={styles.txtDashboard}>DashBoard</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => testeLocal()}>
          <Text style={styles.txtDashboard}>Func</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => userLocal()}>
          <Text style={styles.txtDashboard}>user</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => load()}>
          <Text style={styles.txtDashboard}>balance</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => testeLocal2()}>
          <Text style={styles.txtDashboard}>Month</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnDashboard} onPress={() => updateProfileFB({displayName:"Rodrigo's"})}>
          <Text style={styles.txtDashboard}>updateProfile</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Hello World!</Text>

              <TouchableOpacity
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Hide Modal</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <TouchableOpacity
          style={[styles.button, styles.buttonOpen]}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.textStyle}>Show Modal</Text>
        </TouchableOpacity>
      </View>
    </View>  

  );

}
