import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import BaseNavigation from './src/components/navigation';
import DrawerNavigation from './src/components/DrawerNavigation';

export default function App() {
  return (
    <NavigationContainer>
      <BaseNavigation />
    </NavigationContainer>

  );
}
